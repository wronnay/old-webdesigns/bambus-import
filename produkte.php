<?php
$titel = Produkte;
$description = "Die Produkte der Bambus Import GmbH";
$keywords = "Bambus, Import, Produkte, Textil-Bambus, Music-Bambus, Food-Bambus, Bambus-Pflanzen";
include 'inc/header.php';
?>
<section class="text2">
<article>
<h3>Textil-Bambus</h3>
<ul>
  <li>Speziell f�r die Textilindutrie verarbeitetes Bambus-Bastfaser-Garn</li>
  <li>Aus biologischem Anbau</li>
  <li>Der perfekte Rohstoff f�r Textilien wie Str�mpfe, T-Shirts und Unterw�sche.</li>
</ul>
</article>
 <article>
  <h3>Music-Bambus</h3>
<ul>
<li>Perfekt zur Herstellung von Schlitztrommeln oder von Bambusfl�ten.</li>
</ul>
 </article>
</section>
<section class="text">
 <article>
  <h3>Bambus-Pflanzen ...</h3>
... sind der perfekte Dekorationsgegenstand.<br>
 </article>
 <article>
  <h3>Food-Bambus</h3>
<ul>
<li>Junge Bambussprossen aus biologischen Anbau</li>
<li>Viele Einsatzm�glichkeiten in der K�che</li>
</ul>
 </article>
</section>
<?php
include 'inc/footer.php';
?>