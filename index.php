<?php
$titel = Startseite;
$description = "Bambus online kaufen und mehr: die Internetseite der Bambus Import GmbH";
$keywords = "Bambus, Import, online, kaufen, China, shop";
include 'inc/header.php';
?>
<section class="text2">
<article style="background-image : url(pics/snack.png);
background-repeat : no-repeat;
background-position : right top;">
<h3>Warum Bambus?</h3>
Bambus ist vielseitig verwendbar: als Nahrungsmittel, als Möbelstück, als Textil, als Musikinstrument oder auch als Waffe.
</article>
<article style="background-image : url(pics/lkw.png);
background-repeat : no-repeat;
background-position : right top;">
<h3>Handeln?</h3>
Wir suchen ständig Handelspartner in China und Deutschland. Falls Sie ein Handels-Unternehmen sind, können Sie uns unter: handel@bambusimport.com erreichen.
</article>
 <article style="background-image : url(pics/ort.png);
background-repeat : no-repeat;
background-position : right top;">
  <h3>Wir vor Ort:</h3>
Unser Standort ist in der Musterstraße 12, 12345 Musterstadt. Dort verkaufen wir unseren Bambus direkt aus der Lagerhalle.<br>
 </article>
</section>
<section class="text">
 <article>
  <h3>Willkommen!</h3>
Wir von der Bambus Import GmbH, importieren für Sie hochwertigen Bambus aus China.
 </article>
 <article>
  <h3>Sonderangebot:</h3>
Bis zum 25. Juni bieten wir 5 Pflanzen für nur 60 Euro an.<br>
 </article>
 <article style="background-image : url(pics/kaufen.png);
background-repeat : no-repeat;
background-position : right top;">
  <h3>Kaufen:</h3>
Wenn Sie Bambus für Ihre Wohnausstattung oder Ihren Garten kaufen wollen, dann kontaktieren Sie uns bitte unter: kaufen@bambusimport.com<br>
 </article>
 <article style="background-image : url(pics/frage.png);
background-repeat : no-repeat;
background-position : right top;" >
  <h3>Noch Fragen?</h3>
Sollten Sie noch Fragen haben, können Sie uns per Telefon (09522 / 1234) oder per E-Mail (frage@bambusimport.com) erreichen.<br>
 </article>
</section>
<?php
include 'inc/footer.php';
?>