# Bambus Import

![](screenshot-2014-01-21.png)

**License:** GNU GPL v3

## Background

I made this during an exploration phase when I was interested to become a "Mediengestalter Digital und Print". (I am a Software Developer now)

Realized with PHP, CSS and HTML, the company name is fictional.