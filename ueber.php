<?php
$titel = "�ber uns";
$description = "Informationen �ber die Bambus Import GmbH - von 1992 bis jetzt.";
$keywords = "Bambus, Import, �ber, Uns, Firmengeschichte";
include 'inc/header.php';
?>
<section class="text2">
<article>
<h3>Warum Bambus Import?</h3>
Unsere Produkte sind perfekt auf das jeweilige Einsatzgebiet ausgerichtet. So wird unser <b>Food-Bambus&reg;</b> in den Luxus-Restaurants Frankreichs angeboten, unser <b>Textil-Bambus&reg;</b> wird zu hochwertigen Tommy Hilfiger
 Bio Str�mpfen verarbeitet und unser <b>Music-Bambus&reg;</b> findet steigender Beliebtheit bei der Schlitztrommel Produktion.
</article>
</section>
<section class="text">
 <article>
  <h3>�ber uns:</h3>
  <ul>
<li>Seit 1992 importieren wir f�r Sie Bambus aus China.</li>
<li>Seit 1996 sind wir das gr��te Bambus-Import-Unternehmen in Deutschland.</li>
<li>Seit 1999 bieten wir neben Bambus-Pflanzen auch speziell f�r das jeweilige Einsatzgebiet verarbeitete Bambus-Produkte an.</li>
<li>Aktuell haben wir Standorte in Deutschland, China, Frankreich, Niederlande und Polen.</li>
</ul>
 </article>
</section>
<?php
include 'inc/footer.php';
?>