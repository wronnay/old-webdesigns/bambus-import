<?php
$titel = Datenschutzerklärung;
$description = "Die Datenschutzerklärung der Bambus Import GmbH";
$keywords = "Bambus, Import, Datenschutz, Datenschutzerklärung, Speicherung";
include 'inc/header.php';
?>
<section class="text2">
 <article>
<h3>Bambus Import GmbH</h3>
Deutschland<br>
12345 Musterstadt<br>
Musterstraße 12<br>
09522 / 1234<br>
info@bambusimport.com
 </article>
</section>
<section class="text">
 <article>
  <h3>Datenschutzerklärung:</h3>
    <p>Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, 
      Anschrift oder eMail-Adressen) erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben.   
    </p>
    <p>Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. 
      bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. 
      Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht 
      möglich. </p>
    <p>Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten 
      durch Dritte zur Übersendung von nicht ausdrücklich angeforderter 
      Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. 
      Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte 
      im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, 
      vor.</p><br />
<p><i>Quellenangaben: <a href="http://www.e-recht24.de/muster-disclaimer.htm" target="_blank">Disclaimer</a> von eRecht24, dem Portal zum Internetrecht von Rechtsanwalt Sören Siebert</i></p>
 </article>
</section>
<?php
include 'inc/footer.php';
?>