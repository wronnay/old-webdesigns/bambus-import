<!DOCTYPE html>
<!--
Design by Christoph Miksche
Website: http://celzekr.webpage4.me
License: Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0)

Dieses Werk bzw. Inhalt steht unter einer Creative Commons Namensnennung-Nicht-kommerziell-
Weitergabe unter gleichen Bedingungen 3.0 Unported Lizenz.

Sie duerfen den/die Link/s zu celzekr.webpage4.me nicht entfernen!

(http://creativecommons.org/licenses/by-nc-sa/3.0/)
-->
<html>
<head>
<title><?php echo $titel; ?> - Bambus Import</title>
<link rel="stylesheet" href="css/main.css" type="text/css" />
<meta name="description" content="<?php echo $description; ?>" />
<meta name="keywords" content="<?php echo $keywords; ?>" />
<meta name="author" content="Christoph Miksche" />
</head>
<body lang="de">
<header>
<div class="logo">
<a href="index.php"><img alt="Logo" title="Logo" src="pics/logo.png"></a>
</div>
<div class="login">
<form action="login.php" method="post">
          <input type="text" name="username" value="Username" onclick="if(this.value && this.value==this.defaultValue)this.value=''" size="10"><br>
          <input type="password" name="password" value="Passwort" onclick="if(this.value && this.value==this.defaultValue)this.value=''" size="10"><br>
          <input name="submit" type="submit" value="Login">
      </form>

</div>
</header>
<nav>
<a <?php if($titel=="Startseite") {echo 'class="aktuell"';} ?> href="index.php">Startseite</a> <a <?php if($titel=="�ber uns") {echo 'class="aktuell"';} ?> href="ueber.php">&Uuml;ber uns</a> <a <?php if($titel=="Produkte") {echo 'class="aktuell"';} ?> href="produkte.php">Unsere Produkte</a> <a <?php if($titel=="Registrierung") {echo 'class="aktuell"';} ?> href="registrieren.php">Registrieren</a> <a <?php if($titel=="Kontakt") {echo 'class="aktuell"';} ?> href="kontakt.php">Kontakt</a>
</nav>
<div id="content">
