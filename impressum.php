<?php
$titel = Impressum;
$description = "Das Impressum der Bambus Import GmbH";
$keywords = "Bambus, Import, Impressum, Disclaimer, Haftungsausschluss";
include 'inc/header.php';
?>
<section class="text2">
<article>
<h3>Haftung f�r Inhalte</h3>
    <p>Die Inhalte unserer Seiten wurden mit gr��ter Sorgfalt erstellt. 
      F�r die Richtigkeit, Vollst�ndigkeit und Aktualit�t der Inhalte 
      k�nnen wir jedoch keine Gew�hr �bernehmen. Als Diensteanbieter sind wir gem�� � 7 Abs.1 TMG f�r 
      eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. 
      Nach �� 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht 
      verpflichtet, �bermittelte oder gespeicherte fremde Informationen zu 
      �berwachen oder nach Umst�nden zu forschen, die auf eine rechtswidrige 
      T�tigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der 
      Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon 
      unber�hrt. Eine diesbez�gliche Haftung ist jedoch erst ab dem 
      Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung m�glich. Bei 
      Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte 
      umgehend entfernen.</p>
    <h3>Haftung f�r Links</h3>
    <p>Unser Angebot enth�lt Links zu externen Webseiten Dritter, auf deren 
      Inhalte wir keinen Einfluss haben. Deshalb k�nnen wir f�r diese 
      fremden Inhalte auch keine Gew�hr �bernehmen. F�r die Inhalte 
      der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der 
      Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung 
      auf m�gliche Rechtsverst��e �berpr�ft. Rechtswidrige 
      Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente 
      inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte 
      einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen 
      werden wir derartige Links umgehend entfernen.</p>
    <h3>Urheberrecht</h3>
    <p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten 
      unterliegen dem deutschen Urheberrecht. Die Vervielf�ltigung, Bearbeitung, Verbreitung und 
      jede Art der Verwertung au�erhalb der Grenzen des Urheberrechtes bed�rfen 
      der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads 
      und Kopien dieser Seite sind nur f�r den privaten, nicht kommerziellen 
      Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, 
      werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche 
      gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. 
      Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>
<br>
<p><i>Quellenangaben: <a href="http://www.e-recht24.de/muster-disclaimer.htm" target="_blank">Disclaimer</a> von eRecht24, dem Portal zum Internetrecht von Rechtsanwalt S�ren Siebert</i></p>
</article>
</section>
<section class="text">
 <article>
<h3>Bambus Import GmbH</h3>
Deutschland<br>
12345 Musterstadt<br>
Musterstra�e 12<br>
09522 / 1234<br>
info@bambusimport.com
 </article>
</section>
<?php
include 'inc/footer.php';
?>